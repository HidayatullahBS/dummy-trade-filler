FROM openjdk:8-jdk-alpine

COPY dummy-trade-filler/build/libs/trade-simulator-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8089

# Uncomment to run on docker; Leave commented out to run on localhost
ENV DB_HOST=tradedb:27017
ENV DB_NAME=tradedb
ENV SERVER_PORT=8089

ENTRYPOINT ["java", "-jar", "/app.jar"]
